//
//  ViewController.swift
//  Numeros
//
//  Created by Jenny Castro on 11/27/18.
//  Copyright © 2018 Jenny Castro. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBOutlet weak var txtUser: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func btnLoging(_ sender: Any) {
        let username = txtUser.text!
        let password = txtPassword.text!
        
        Auth.auth().signIn(withEmail: username, password: password)
        {
            (data, error) in
            if let error = error {
                print(error)
                return
            }
            //print("Welcome...!!")
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
    }
    
    
}

